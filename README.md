# Client Side Library Resolver

This is a small library which is designed to allow you to switch between using client-side libraries saved under `node_modules` and ones hosted on a CDN.

I'll throw up a tutorial when I have some free time, but until then you can check out the [API reference](https://client-side-library-resolver.docs.zacharyboyd.nyc) or look at the Mocha test under `test/`.

## Building

This library is written in TypeScript, to compile run `npm run build`.

## Docs

You can generate docs by running `npm run docs`.

## Testing

Mocha tests are available under `test/` run with `npm test`.